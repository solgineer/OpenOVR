Hand models

Taken from https://developer.oculus.com/downloads/package/oculus-hand-models/
Licence: CC-BY 4.0 (Attribution required)

These Maya models were exported without modification as OBJs, from Maya 2016.

Only normals were enabled
